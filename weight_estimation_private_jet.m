clc 
clear all
%INPUTS
%--------------------------------------------------------------------------
Num_engine=2;
%Trated=2e6; %N (Mirar Nil)
%Wengine=(Trated-153.6)/5646; %Mirar del NIL
Wengine=Num_engine*200; %Mirar NIL
%Ascens
v_climb=12.7*3.6; %km/s
v_avanc_climb=130*3.6; %km/h  %Revisar aquest valor pel jet
altitude = 42000*(1/3.28084)/1000; %ft to meter to km
%Mission
range_mission=2000; %km (ull unitats!)
v_cruise=175*3.6; %km/h --> posar valor coherent
Range_alternate = 200; %km %Ull que �s a pelo!!!!!
v_alternate=128*3.6; %km7h, max FAA REVISAR!

t_seguretat=5/60; %hours
%Excel results exportation
filname='Mass_Private_Jet_2.xlsx';
sheet = 1;
xlRange ='T22:U22';
data_coef = xlsread(filname,sheet,xlRange);
oew_mtow=data_coef(1);
fw_mtow=data_coef(2);

%NOTA: Agafo dades de l'Honda-Jet fins que el maty m'arregli l'Excel
mtow_honda=4853;
oew_honda= 3303;
fw_honda= 816.46;
%oew_mtow = oew_honda/mtow_honda;
%fw_mtow = fw_honda/mtow_honda;

%PAYLOAD ESTIMATION %Explain that we will use a configuration of 6+1 pilot
%or 5+2 pilots 
n_pax = 6;
n_crew = 2;
weight_pax = 89; %Nil's reference
weight_crew = 77;

%Can be changed and discussed:
baggage = 13; %Between 12 and 20

PL = n_pax*(weight_pax+baggage);
WCREW=n_crew*(weight_crew+baggage);

%MPL CALCULATION
rho_merc = 997; %Buscar
rho_equipaje =12/(50600e-6); %m^3
kbd = 0.8; %Es podria baixar
sb=147773e-6; %m^2;
lbodega=7; %m;
Vb =sb*lbodega ; %Dep�n del fuselatge

MPL = n_pax*(weight_pax+baggage)+rho_merc*(kbd*Vb-n_pax*baggage/rho_equipaje);

error=1;
n=1;
tolerancia=0.05;
%FIRST MTOW APPROX.
%MTOW CALCULATION
%Torenbeek reference: 
MTOW_min=(PL+WCREW)/(1-oew_mtow-fw_mtow);

 %OEW
 %--------------------------------------
 %OEW CALCULATION
%---------------------------------------------------------
%Fuselage geometry for Torenbeek's graph calculation (p.147)
lf= 10.4;%Lenght fuselage
bf= 2.278; %Height fuselage %Not considering the lower part
hf= 2.174;%Width fuselage
valor_x=lf*(bf+hf)/2;
delta_we=130.5*valor_x-1110; %By choosing two different points of the graph Fig 5-3
%Roskman constants (FONT: p.47, tab. 2.15 )
A=0.2678;
B=0.9979;
%Correcci� composites
%S'hauria de mirar el pes de cada component i multiplicar segons taula 2.16
%De moment agafem un valor a oju
correct_rosk=0.4;
 %Loop to find the ideal value for MTOW, by using Torenbeek reference for
 %Note that Roskam configuration was discared because it was too highly
 %compared with others
 
 %------------------------------------------------------------------------
 %FW CALCULATION --> Flight profile
 %PHASE 4: CLIMB TO CRUISE ALTITUDE AND ACCELERATE TO CRUISE SPEED
%------------------------------------------------------------------
%w4=w_phases(4)*w3; %Better use Breguet because it is an important range
t_climb=altitude/v_climb;
range_climb=t_climb*v_avanc_climb; %km (revisar unitats eeh!)

%PHASE: LOITER
%-------------------------------------------------------
%Breguet's Endurance equation
%CONSTANTS (Canviar de lloc) From Roskam
E_ltr = 0.75; %hours of loiter
c_j_ltr=0.5*0.4535/1; %(lb to kg)/hour
L_D_ltr=13;

%FW reserva
fw_reserva=0.15; % en percentatge Buscar valor de normativa
while error>tolerancia
    n=n+1;
%OEW CALCULATION (Torenbeek)
OEW(1)= 0.2*MTOW_min+500+delta_we+Wengine; %Pendent (potser seria millor posar valor regressi�) (Mirar valor que surt del llibre)
%Semejantes and also Snorri Gudmundsson
OEW(2)=MTOW_min*oew_mtow;
%Roskam
OEW(3) = correct_rosk*(10^(A)*MTOW_min^(B)); %Revisar, perqu� surt realment desorbitat
%EW_permitted= correct_rosk*log10((MTOW_min^((B^(-1)))))*10^(-A/B);
%--------------------------------------------------------------------------

%(FROM Snorri Gudmundsson)[TAKE CARE, IT IS FOR A TURBOPROP GA!)
%SECOND METHODOLOGY
OEW_MTOW_2=0.5371+0.0066*log(MTOW_min); %p.148 Snorri, per� turboprop.
OEW(4)=MTOW_min*OEW_MTOW_2;

%--------------------------------------------------------------------------
%FW CALCULATION
%-------------------------------------------------------------------------------
%INITIAL APPROACH 
FW(2)=fw_mtow*MTOW_min;
%Flight profile (dep�n de Breguet, caldria mirar bibliografia)
%See mission profile p.55 Roskam

%PHASE 1: Engine Start and Warm Up
w_phases=[0.990;0.995;0.995;0.980;0;0;0.990;0;0.992];
w1=w_phases(1)*MTOW_min;

%PHASE 2: TAXI
w2=w_phases(2)*w1;

%PHASE 3: TAKE-OFF
w3=w_phases(3)*w2;

%PHASE 4: CLIMB TO CRUISE ALTITUDE AND ACCELERATE TO CRUISE SPEED
%------------------------------------------------------------------
%hauria d'entrar a breguet en aquest punt.
%Input = w3 ; range_climb
%Output = w4
w4 = funct_FW(range_climb,w3,v_avanc_climb,0.5*0.453592,13); %Ull que L/D surt de taules

%-----------------------------------------------------------------------
%PHASE 5: CRUISE (BREGUET FUNCTION)
%Also is a Breguet procedure
%Input: w4; range_mission
%Output: w5
%Include 5 more minutes of cruise

w5 = funct_FW(range_mission+v_cruise*t_seguretat,w4,v_cruise,0.5*0.453592,11);
%--------------------------------------------

%PHASE 6: LOITER
w6=w5*exp(-E_ltr*c_j_ltr/L_D_ltr);

%PHASE 7: DESCENT
w7=w_phases(7)*w6;

%PHASE RESERVE: LOITER_RESERVA  %CS23, 45 more extra minutes
w_reserva=w7*exp(-E_ltr*c_j_ltr/(0.95*L_D_ltr));
%PHASE 8: FLY TO ALTERNATE AND DESCENT
w8 = funct_FW(Range_alternate,w_reserva,128,0.9*0.453592,10);
%Variables: L/D_max = 10; cj=0.9; v_max = 128 m/s (FAA)
%Inputs: %Range_alternate; w7
%Outputs: w8;

%PHASE 9: LANDING, TAXI, SHUTDOWN
w9=w_phases(9)*w8;

%USED FUEL
FW(1)=MTOW_min-w9;

OEW_tent=MTOW_min-FW(1)-PL;
EW_tent=OEW_tent-WCREW-0.05*MTOW_min;
MTOW=FW(1)+OEW_tent+PL+WCREW;
error=abs(OEW_tent-OEW(1))/OEW(1);
%error=abs((EW_tent-EW_permitted))/EW_permitted;
%if error > tolerancia
    %PL=-(OEW(3)-MTOW_min-FW); %Com podria fer una iteraci�?�
 MTOW_min=MTOW;
  %  end
  if MTOW >5.5e3
      break
  end

end

MTOW_def=MTOW
OEW_def=OEW
FW_total=FW(1)
Wi_cruise=w4
Wf_cruise=w5
ratio_mtow_ltow=w8/MTOW
%AUTHOR'S NOTES:
%-Note that the iteration depends on Roskam's proposed methodology by using
%Torenbeek's OEW approach
%The aim of the program is to be able to compare all the results togheter
%and to decide which is better to estimate the weight of the Jet Plane
%Some values may be reviewed for an accurate approximation, specially those
%related to the velocity. 
    
%plot(MTOW,OEW(1))

%plot(MTOW,OEW(2))
%plot(MTOW,OEW(3))
%plot(MTOW,OEW_2(1))
%plot(MTOW,OEW_(2))


