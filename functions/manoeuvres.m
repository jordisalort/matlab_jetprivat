function [Vs1, Va, Vd, Vf, Vs1_neg, Vs_flap, Vd_h] = manoeuvres(MTOW, Sw, Cn_max, Cn_max_TO, Ma_c, c_aero, CL_alpha)

%% Inputs

loadConstants;
g           = 9.81;
rho         = 1.225;   % Air density at sea level
a0          = 343;     % Speed of sound at sea level
n_max_flaps = 2;
n_min       = -1.52;
n_max       = 3.8;  % From class slides for small aircraft
i           = 80;

%% Airspeeds calculation without flaps

stall_factor = 0.5*rho*Sw*Cn_max/(MTOW*g);

% Relevant airspeeds
%Vc = sqrt((2*MTOW*g)/(rho*Sw*Cn_cr)); % Cruise speed at sea level with MTOW and Cn_cr
Vd    = 1.37*Ma_c*a0;                    % Vd according to C 23.335 b)
Vs1   = sqrt(1/stall_factor);            % Stall airspeed for n=1
Va_f  = sqrt(n_max/stall_factor);        % Stall airspeed for n_max
Va    = Vs1*sqrt(n_max);

% Vectors for the plot
V1 = linspace(0,Va_f,i);          % Vector for airspeed from 0 to Va
n1 = stall_factor*V1.^2;

%% Flap curve

stall_factor_TO = 0.5*rho*Sw*Cn_max_TO/(MTOW*g);

% Relevant airspeeds
Vs_flap = sqrt(1/stall_factor_TO);           % Stall speed with flaps
Vf1     = sqrt(n_max_flaps/stall_factor_TO); % Stall airspeed with flaps for n_max_flaps
Vf      = max(1.8*Vs_flap,1.4*Vs1);          % Maximum airspeed with flaps;

% Vectors for the plot
V2 = linspace(0,Vf1,i);
n2 = stall_factor_TO*V2.^2;

%% Negative curve

neg_factor = 0.5*rho*Sw*Cn_max*(-0.8)/(MTOW*g);

% Relevant airspeeds
Vs1_neg = sqrt(n_min/neg_factor); % Stall airspeed for minimum factor (negative)

% Vectors for the plot
V_neg = linspace(0,Vs1_neg,i);
n_neg = neg_factor*V_neg.^2;

%% Full vectors creation (SL)

delta = 0.01;

% Curve without flaps
n_noFlaps = [n1, n_max, n_max, 0];
V_noFlaps = [V1, Va_f+delta, Vd, Vd+delta];

% Curve with flaps
n_flaps = [n2, n_max_flaps, n_max_flaps];
V_flaps = [V2, Vf1+delta, Vf];

% Negative curve
n_inv = [n_neg, n_min, n_min, 0];
V_inv = [V_neg, Vs1_neg+delta, Ma_c*a0, Vd];

%% Manoeuvre diagram plotting (SL)

figure;
plot (V_noFlaps, n_noFlaps,'-k','LineWidth',2); hold on;
plot (V_flaps, n_flaps,'-k','LineWidth',2)
plot (V_inv, n_inv,'-k','LineWidth',2)
xlabel ('V (EAS) [m/s]')
ylabel ('n')
xlim ([0 Vd+20])
ylim ([-2 5])

%% Full vectors creation (43000ft)

Vd_h  = 1.37*Ma_c*a0*f_Theta_sqrt(43000);

% Curve without flaps
n_noFlaps_h = [n1, n_max, n_max, 0];
V_noFlaps_h = [V1, Va_f+delta, Vd_h, Vd_h+delta];

% Curve with flaps
n_flaps_h = [n2, n_max_flaps, n_max_flaps];
V_flaps_h = [V2, Vf1+delta, Vf];

% Negative curve
n_inv_h = [n_neg, n_min, n_min, 0];
V_inv_h = [V_neg, Vs1_neg+delta, Ma_c*a0*f_Theta_sqrt(43000), Vd_h];

%% Manoeuvre diagram plotting (43000ft)

figure;
plot (V_noFlaps_h, n_noFlaps_h,'-k','LineWidth',2); hold on;
plot (V_flaps_h, n_flaps_h,'-k','LineWidth',2)
plot (V_inv_h, n_inv_h,'-k','LineWidth',2)
xlabel ('V (EAS) [m/s]')
ylabel ('n')
xlim ([0 Vd+20])
ylim ([-2 5])

%% Gust diagram

% Inputs
Vc = Ma_c*a0*f_Theta_sqrt(43000);
rho_h = 0.245;  % [kg/m^3] Air density at 43000ft

Ud_D = 4.88;    % [m/s] == 16 ft/s
Ud_C = 9.45;    % [m/s] == 31 ft/s
Ud_B = 13.72;   % [m/s] == 45 ft/s

mu_g = 2*(MTOW/Sw)/(rho_h*c_aero*CL_alpha*g);  % aeroplane mass ratio CS23.337 c)
kg   = 0.88*mu_g/(5.3+mu_g);                   % gust alleviation factor

%% Gusts lines creation

n_D     = 1+kg*(0.5*rho*Vd_h*Ud_D*CL_alpha*Sw)/(MTOW*g);
n_C     = 1+kg*(0.5*rho*Vc*Ud_C*CL_alpha*Sw)/(MTOW*g);

Vb      = Vs1*sqrt(n_C);

n_B     = 1+kg*(0.5*rho*Vb*Ud_B*CL_alpha*Sw)/(MTOW*g);
n_C_neg = 1-kg*(0.5*rho*Vc*Ud_C*CL_alpha*Sw)/(MTOW*g);
n_B_neg = 1-kg*(0.5*rho*Vb*Ud_B*CL_alpha*Sw)/(MTOW*g);
n_D_neg = 1-kg*(0.5*rho*Vd_h*Ud_D*CL_alpha*Sw)/(MTOW*g);

n_D1 = [1, n_D];
n_D2 = [1, n_D_neg];
V_D  = [0, Vd_h];
n_C1 = [1, n_C];
n_C2 = [1, n_C_neg];
V_C  = [0, Vc];
n_B1 = [1, n_B];
n_B2 = [1, n_B_neg];
V_B  = [0, Vb];

%% Gusts plotting

figure;
plot (V_noFlaps_h, n_noFlaps_h,'-k','LineWidth',2); hold on;
plot (V_inv_h, n_inv_h,'-k','LineWidth',2)
plot (V_D,n_D1,'-k', V_D, n_D2, '-k', V_C,n_C1,'-k',V_C,n_C2,'-k',V_B,n_B1,'-k',V_B,n_B2,'-k')
xlabel ('V (EAS) [m/s]')
ylabel ('n')
xlim ([0 Vd+20])
ylim ([-2 5])

%% Diagram vectors considering gusts

% Diagram points

m          = (n_B-1)/Vb;  % Slope of gust line for Vb
gust_b     = @(V) m*V+1;  % Gust line for Vb
gust_b_neg = @(V) -m*V+1; % Gust line for -Vb

boolean_j = false;
boolean_i = false;

for z = 1:1:i
    if n1(z) >= gust_b_neg(V1(z)) && boolean_j == false
        pos_j     = z;
        boolean_j = true;
    elseif n1(z) >= gust_b(V1(z)) && boolean_i == false
        pos_i = z;
        boolean_i = true;
    elseif boolean_j == true && boolean_i == true
        break;
    end
end


%n2 = min(n_B, n_max);
n2 = n_B;
n3 = min(n_C, n_max);
n4 = min(n_D, n_max);
if n_D_neg > 0
    n5 = n_D_neg;
else
    n5 = 0;
end
n6 = max(n_C_neg, n_min);
n7 = max(n_B_neg, n_min);

n_curve12 = n1(pos_j:pos_i);
V_curve12 = V1(pos_j:pos_i);

n_gust_pos = [n_curve12, n2, n3, n4, n5];
V_gust_pos = [V_curve12, Vb, Vc, Vd_h, Vd_h+delta];
n_gust_neg = [n1(pos_j), n7, n6, n5];
V_gust_neg = [V1(pos_j), Vb, Vc, Vd_h+delta];

figure;
plot (V_noFlaps_h, n_noFlaps_h,'-k'); hold on;
plot (V_inv_h, n_inv_h,'-k')
plot (V_D,n_D1,'-k', V_D, n_D2, '-k', V_C,n_C1,'-k',V_C,n_C2,'-k',V_B,n_B1,'-k',V_B,n_B2,'-k')
plot (V_gust_pos, n_gust_pos,'-k','LineWidth',2);
plot (V_gust_neg, n_gust_neg,'-k','LineWidth',2)
xlabel ('V (EAS) [m/s]')
ylabel ('n')
xlim ([0 Vd+20])
ylim ([-2 5])

end

