function [Wi_cruise,Wf_cruise,fuel_reserva]=funct_weight_PLrange(OEW,PL,TF)
%INPUTS
%--------------------------------------------------------------------------
loadConstants;
Ma = 0.69;

%Ascens
v_climb=12.7*3.6; %km/s
v_avanc_climb=130*3.6; %km/h  %Revisar aquest valor pel jet
altitude = Altitud*(1/3.28084)/1000; %ft to meter to km
%Mission
range_mission=2000; %km (ull unitats!)
% v_cruise=175*3.6; %km/h --> posar valor coherent
v_cruise = Ma*a0*f_Theta_sqrt(Altitud)*3.6; %km/h
Range_alternate = 200; %km %Ull que �s a pelo!!!!!
v_alternate=128*3.6; %km7h, max FAA REVISAR!

t_seguretat=5/60; %hours
 

%PHASE: LOITER
%-------------------------------------------------------
%Breguet's Endurance equation
%CONSTANTS (Canviar de lloc) From Roskam
E_ltr = 0.75; %seconds of loiter
c_j_ltr=0.5; %(lb to kg)/s
L_D_ltr=13;
%TOW CALCULATION
ratio_reserva=exp(-E_ltr*c_j_ltr/(L_D_ltr));
fuel_reserva=(1-ratio_reserva)*(OEW+PL);
TOW=OEW+PL+TF+fuel_reserva;
 %------------------------------------------------------------------------
 %FW CALCULATION --> Flight profile
 %PHASE 4: CLIMB TO CRUISE ALTITUDE AND ACCELERATE TO CRUISE SPEED
%------------------------------------------------------------------
%w4=w_phases(4)*w3; %Better use Breguet because it is an important range
t_climb=altitude/v_climb;
range_climb=t_climb*v_avanc_climb; %km (revisar unitats eeh!)


  
%Flight profile (dep�n de Breguet, caldria mirar bibliografia)
%See mission profile p.55 Roskam

%PHASE 1: Engine Start and Warm Up
w_phases=[0.990;0.995;0.995;0.980;0;0;0.990;0;0.992];
w1=w_phases(1)*TOW;
%PHASE 2: TAXI
w2=w_phases(2)*w1;
%PHASE 3: TAKE-OFF
w3=w_phases(3)*w2;
%PHASE 4: CLIMB TO CRUISE ALTITUDE AND ACCELERATE TO CRUISE SPEED
%------------------------------------------------------------------
w4 = funct_FW(range_climb,w3,v_avanc_climb,0.5,13); %Ull que L/D surt de taules



LW=TOW*(w_phases(1)*w_phases(2)*w_phases(3)*(w4/w3)*w_phases(9)*w_phases(7)*exp(-E_ltr*c_j_ltr/L_D_ltr))-TF;
%PHASE 9: LANDING, TAXI, SHUTDOWN
w8=LW/w_phases(9);
%PHASE 7: DESCENT
w7=w8/w_phases(7);
%PHASE 6: LOITER
w6=w7/exp(-E_ltr*c_j_ltr/L_D_ltr);


Wi_cruise=w4;
Wf_cruise=w6;
end


