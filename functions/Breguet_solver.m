%g: gravity [m/s^2]
%c_j: Thrust-specific fuel consumption [kg/(s*N)]
%T: Thrust [N]
%v: velocity [m/s]
% e == L/D 10-12 for business jets
conversion_cj = 0.45359/3600/4.4482;

%% Inputs

g=9.81;         c_j=0.5*conversion_cj;      v=243;    
W_i=3900;        W_f=3100;      rend=0.8;   efic=16;

%Breguet function
f=@(W) -v*rend*efic/(g*c_j*W);

%Numerical methods parameters
a=W_i;
b=W_f;
n=20;
h=(b-a)/n;

%% Trapezoidal rule

% Using a uniform grid:
s=0.5*(f(a)+f(b));
for i=1:n-1
   s=s+f(a+i*h);
end
R_1  = h*s;
Rang1_km = R_1/1000;
k_tf1_km = Rang1_km/log(a/b);

%Simpsons rule
s=f(a)+f(b);
for i=1:2:n-1
    s=s+4*f(a+i*h);
end
for i=2:2:n-2
    s=s+2*f(a+i*h);
end
R_2=h/3*s;
Rang2_km = R_2/1000;
k_tf2_km = Rang2_km/log(a/b);

%% Error calculation

% Trapezoidal
% https://math.stackexchange.com/questions/2286982/trapezoidal-method-integration-and-error-estimation
f2=@(W) -2*v*rend*efic/(g*c_j*W^3);

for i=1:1:n
    y_max=0;
    W=linspace(a,b,i);
    for j=1:1:i
    y=f2(W(1,j));
        if abs(y)>y_max
            y_max=abs(y);
        end
    end
    error_t(1,i)=y_max*(b-a)^3/(12*i^2);
end
error_t=abs(error_t);

%Simpsons
%http://www.math.pitt.edu/~sparling/23021/23022numapprox2/node6.html
f4=@(W) -24*v*rend*efic/(g*c_j*W^5);
for i=1:1:n
    y_max=0;
    W=linspace(a,b,i);
    for j=1:1:i
    y=f4(W(1,j));
    if abs(y)>y_max
        y_max=abs(y);
    end
    end
    error_s(1,i)=y_max*(b-a)^5/(180*i^4);
end
error_s=abs(error_s);

%% Plotting

figure;
semilogy(error_t);
hold on;
semilogy(error_s);
legend('Trapezoidal Rule','Simpsons Rule')
xlabel('Order of discretization')
ylabel('Absolute error')
grid on;
hold off
