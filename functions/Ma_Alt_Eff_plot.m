%% Flight Altitudes and Ma numbers

Alt = linspace(15000,60000,10);
Ma  = linspace(0.3,0.7,8);

%% 
loadConstants;

% Based on other business jets, taking into account that the efficiency is
% usually between 10 and 12 (taken as 11), the Cl goes between 1.4 and 1.8
% (taken as 1.6), and Ma is 0.6 at this conditions.

% Increase of resistance coefficient due to Mach number
 delta_Cd_Ma = @(Ma) (Ma<=0.6).*(0)+(Ma>0.6).*0.1*(Ma-0.6);

% Cl/Cd taking Cl as 11, Cd as a result of assuming E=11 at 40000ft, and
% considering a drag increase of 20% in 30000ft from 15000ft and also
% considering the increase due to Mach number
efic = @(Alt,Ma) 1.6/(0.145/1.2*(1+0.2/30000*Alt)+delta_Cd_Ma(Ma));


%% Loop for all Altitudes

for i = 1:1:length(Alt)
    
    for j = 1:1:length(Ma)
       eficiencia(i,j)= efic(Alt(i),Ma(j)); 
    end
    
end

%% Plotting

figure;
surf (Ma, Alt, eficiencia);
xlabel ('Ma'); ylabel ('Altitude (ft)'); zlabel ('Efficiency');
