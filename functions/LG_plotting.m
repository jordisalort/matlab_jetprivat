function LG_plotting(LG_height, track, NLG_pos, MLG_pos, es)

figure;

%---------------------------------------------------------------
subplot(2,2,1)
plot(es, LG_height, '-k')
grid on
xlabel ('Static deflection [m]')
ylabel ('LG height [m]')
%---------------------------------------------------------------
subplot(2,2,2)
plot(es, track, '-k')
grid on
xlabel ('Static deflection [m]')
ylabel ('Track [m]')
%---------------------------------------------------------------
subplot(2,2,3)
plot(es, NLG_pos, '-k')
grid on
xlabel ('Static deflection [m]')
ylabel ('NLG position [m]')
%---------------------------------------------------------------
subplot(2,2,4)
plot(es, MLG_pos, '-k')
grid on
xlabel ('Static deflection [m]')
ylabel ('MLG position [m]')



end

