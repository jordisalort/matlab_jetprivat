function [ESWL1, ESWL_St, ESWL_P] = LCN_analysis(MTOW)

%% Inputs

n_P      = 4;
P        = linspace(10,25,n_P);
P_fixed  = 11;
n_St     = 4;
St       = linspace(30,63,n_St);
St_fixed = 30;

z = 200;
thickness = linspace(1,250,z);

%% 1 wheel per leg

L_leg = MTOW*0.885/2;
ESWL1 = L_leg;

%% 2 wheels per leg

L_wheel = L_leg/2;

ESWL=@(x, St, D_2) L_wheel.*(x<log10(D_2))+((L_wheel-L_leg)/(log10(D_2)-log10(2*St))*x+L_leg+(L_leg-L_wheel)/(log10(D_2)-log10(2*St))*log10(2*St)).*((x>=log10(D_2))&(x<=log10(2*St)))+L_leg.*(x>log10(2*St));

for i = 1:1:n_St               % Loop for different St distances
    D_2    = St(i)/2-sqrt(L_wheel/(1.4*pi*P_fixed)); % [cm] P= 25 kg/cm^2
    for m = 1:1:z  % This loop draws the ESWL curve
        ESWL_St(i,m) = ESWL(log10(thickness(m)), St(i), D_2);
    end
end

for i = 1:1:n_P               % This loop takes half the combinations studies for different e_s
    D_2    = St_fixed/2-sqrt(L_wheel/(1.4*pi*P(i))); % [cm] St= cm
    for m = 1:1:z  % This loop draws the ESWL curve
        ESWL_P(i,m) = ESWL(log10(thickness(m)), St_fixed, D_2);
    end
end

%% Plotting

figure;
X1 = (['S_T = ', num2str(St(1)), ' [cm]']);
X2 = (['S_T = ', num2str(St(2)), ' [cm]']);
X3 = (['S_T = ', num2str(St(3)), ' [cm]']);
X4 = (['S_T = ', num2str(St(4)), ' [cm]']);

semilogx(thickness,ESWL_St(1,:),'-k'); hold on; grid on;
semilogx(thickness,ESWL_St(2,:),'--k')
semilogx(thickness,ESWL_St(3,:),':k')
semilogx(thickness,ESWL_St(4,:),'-.k')
xlabel('Total thickness of pavement [cm]')
ylabel('Equivalent Single Wheel Load [kg]')
xlim ([0 (2*St(n_St)+50)])
legend(X1, X2, X3, X4, 'Location','southeast')

%-------------------------------------------------------------------------------------------------

figure;

X1 = (['P = ', num2str(P(1)), ' [kg/cm^2]']);
X2 = (['P = ', num2str(P(2)), ' [kg/cm^2]']);
X3 = (['P = ', num2str(P(3)), ' [kg/cm^2]']);
X4 = (['P = ', num2str(P(4)), ' [kg/cm^2]']);

semilogx(thickness,ESWL_P(1,:),'-k'); hold on; grid on;
semilogx(thickness,ESWL_P(2,:),'--k')
semilogx(thickness,ESWL_P(3,:),':k')
semilogx(thickness,ESWL_P(4,:),'-.k')
xlabel('Total thickness of pavement [cm]')
ylabel('Equivalent Single Wheel Load [kg]')
xlim ([0 (2*St_fixed+50)])
legend(X1, X2, X3, X4, 'Location','southeast')


end
