% This function  all LG parameters relevant in a preliminary
% design, as well as the wheel loads and the LCN

function [LG_height, track, NLG_pos, MLG_pos, contador] = LG (b, dihedral, sweep, A, Xcg_for_MTOW, Xcg_aft_MTOW, es)
%% Inputs:
% b        = wingspan       [m]
% dihedral = dihedral angle [deg]
% sweep    = sweep angle    [deg]
% A        = Aspect Ratio
% a        = Distance between the Xac and CDG_for [m].+ value. TBD
a          = Xcg_for_MTOW; % EXAMPLE. TO BE REPLACED
% h_cg     = height of the CDG
ny         = 0.5;
k_sg       = 1/2;

cdg_envelope = abs(Xcg_for_MTOW-Xcg_aft_MTOW); % Distance between the extreme positions of the CDG
y            = a+cdg_envelope;                 % Distance between the Xac and CDG_aft [m]

% Approximation for guard angle considering no slats (Torenbeek, page 351)
guard = 7*(1+3/A); % [deg]

pitch_TD = guard; % [deg] pitch angle when touchdown (Supposed the same as guard angle, assumption based on Torenbeek page 353)

delta = 0.001;     % Acceptable difference on the MLG position

Track_loop = false;

% Supposed value for the distance between wheels
t_sup = 0.5; % [m]

contador = 1;

%% Beggining of the loop for the track calculation

while Track_loop == false
    
    
%% MLG location

% Wheels height due to the 8 deg restriction. 10% of margin applied for
% downward movement of wing tip (Torenbeek, page 350)
LG_height = (b-t_sup)/2*(tand(8)-tand(dihedral)+tand(guard)*tand(sweep))*1.1; % [m]

% MLG distance after the maximum aft possible CDG position (distance
% between last possible CDG and MLG)
lm = (LG_height+es)*tand(pitch_TD);
lm = 1.1*lm; % Margin of 10%

MLG_pos = y+lm;

% ----NLG LOCATION----------------------------------------

% Distances measured from the Xac
ln1 = (lm+cdg_envelope)*0.92/0.08 - a;
ln2 = lm*0.85/0.15 - y;

NLG_pos = (ln1+ln2)/2; % NLG at the mid point between NLG_aft and NLG_for.

%----------------------------------------------------------

h_cg = LG_height+1; % CDG height is 1m above the LG

% Radius determination
r = ny * h_cg * (1+4*k_sg*es*h_cg/(t_sup^2));

% Distances between NLG and CDGs
x     = NLG_pos + a;   % Distance between the extrem forward position of the CDG and the NLG
ln    = NLG_pos + y;   % Distance between the NLG and the extrem aft position of the CDG

% Track claculation
angle = asin(r/x);
track = 2*tan(angle)*(ln+lm);
track = 1.2*track;

%% Track verification

if abs(track-t_sup) > delta
    t_sup = track;
    contador = contador + 1;
else 
    Track_loop = true;
end

end


end

