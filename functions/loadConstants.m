

g          = 9.81;      % gravity. 
consum_esp = 0.7;       % Typical value for business jets. In English units. Conversion is below to SI.
vel_creuer = 200;       % This value shouldn't be used. It is preferable to have Mach number and altitude as inputs for velocity
Massa_i    = 3900;      % Random value. This should be an output of other functions. Use it just to validate your script.
Massa_f    = 3000;      % Random value. This should be an output of other functions. Use it just to validate your script.
rend_jet   = 0.75;       % This value has not been justificated or validated.
efic       = 11;        % Efficiencies during cruise phase for business jets usually go from 10 to 12
a0         = 340;       % Reference of speed of the sound
cj_ref     = 0.552;       % Specific fuel consumption at a Ma_ref. In English units. Conversion is below to SI. [lb/(lb*h)] (Roskam reference)
Ma_ref     = 0.69;       % Mach number reference for cj_ref
beta_ct    = 0.5;       % Constant from Theory sessions for cj
Altitud    = 43000;     % Cruise altitude of similar business jets
Sr         = 7;         % Slenderness ratio for the fuselage

%% Variation of Sqrt Theta with Altitude
% Ref: Page 572 of Torenbeek

Alt_ft     = [5000 10000 15000 20000 25000 30000 35000];
Sqrt_Theta = [.98266 .96501 .94703 .92870 .91001 .89092 .87141];

p = polyfit (Alt_ft, Sqrt_Theta, 1);

f_Theta_sqrt = @(Alt) (Alt<=35000).*(p(1)*Alt+p(2))+(Alt>35000).*(.86710);


%% Changing units for specific fuel consumption. From English system to SI

conv_cj = 1/3600/4.4482; %lb/h to lb/lbf*h to kg/Ns
c_j_ref = conv_cj*cj_ref; %kg/(N*s)

