% Script that gives a 3-D plot with Mach number, altitude and range

function [Range] = Range_loop(W_i, W_f)

loadConstants;
%% Flight Altitudes and Ma numbers

Alt = linspace(15000,60000,10);
Ma  = linspace(0.3,0.7,8);

%% Loop for all Altitudes

for i = 1:1:length(Alt)
    
    for j = 1:1:length(Ma)
       [~,Range(i,j)]= Breguet_singleMaAlt(Ma(j), Alt(i), efic, W_i, W_f); 
    end
    
end

%% Plotting

figure;
surf (Ma, Alt, Range);
xlabel ('Ma'); ylabel ('Altitude (ft)'); zlabel ('Range(km)');

end


