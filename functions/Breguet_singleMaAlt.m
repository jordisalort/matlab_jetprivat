% Function for Breguet solver as a function of Ma and Altitude

function [ Rang_trapez_km, Rang_simpson_km] = Breguet_singleMaAlt(Ma, Alt, efic, W_i, W_f)

loadConstants;
% Cl/Cd taking Cl as 11, Cd as a result of assuming E=11 at 40000ft, and
% considering a drag increase of 20% in 30000ft from 15000ft and also
% considering the increase due to Mach number

%% Breguet function definition
f=@(W) -a0*f_Theta_sqrt(Alt)*efic*(Ma_ref^(beta_ct))*(Ma^(1-beta_ct))/(g*c_j_ref*W);

%% Breguet inputs

a=W_i;
b=W_f;
n=2;
h=(b-a)/n;

%% Breguet solver using Simpsons rule

s=f(a)+f(b);
for i=1:2:n-1
    s=s+4*f(a+i*h);
end
for i=2:2:n-2
    s=s+2*f(a+i*h);
end
R_2=h/3*s;
Rang_simpson_km = R_2/1000;
Rang_trapez_km = Rang_simpson_km;

end

