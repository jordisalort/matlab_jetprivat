function [R_t] = Trapezi(a,b,n,f)

h=(b-a)/n;
s=0.5*(f(a)+f(b));
for i=1:n-1
   s=s+f(a+i*h);
end
R_t  = h*s;

end