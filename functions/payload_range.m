% This function outputs two plots: one with the range for a given weight
% configuration, and the other the maximum payload for a given range. This
% function depends on the different aircraft weights (MTOW, OEW, MZFW...)
% and the cruise conditions.

function [Range, Weight_t] = payload_range(MPL, OEW, MTOW, MFW, Ma, Alt, efic)

%% Initial inputs and vectors creation

[~,~,RF_max] = funct_weight_PLrange(OEW,MPL,10);  % Reserve fuel when maximum payload
TF           = MTOW-OEW-MPL-RF_max;               % Trip fuel with MPL and MTOW
PL_min       = MTOW-MFW-OEW;                      % Maximum payload with MFW and MTOW

% Subdivisions for each part of the diagram
a = 5;
b = 10;
c = 5;
n = a+b+c;  % Total of weight configuration for creating the ddiagram

% Weight increases
TF_sub  = TF/(a-1);            % Trip fuel increase for the first part
PL_sub1 = (MPL-PL_min)/b;      % Payload decrease for the second part
PL_sub2 = (PL_min)/c;          % Payload decrease for the third part

% Vectors creation
Weight_t    = 1:n;   % Total weight
Weight_pl   = 1:n;   % Payload weight
Weight_fw   = 1:n;   % Fuel weight
Weight_OEW  = 1:n;   % OEW
Weight_PL   = 1:n;   % PL
Weight_RF   = 1:n;   % RF
Range       = 1:n;   % Range

% Initial weight configuration definition
Weight_t(1)  = OEW+MPL+RF_max;  
Weight_pl(1) = MPL;
Weight_fw(1) = 0;

%% Loop for different weight configurations

for i = 2:1:a
    Weight_pl(i) = Weight_pl(i-1);
    Weight_fw(i) = Weight_fw(i-1)+TF_sub;
    Weight_t(i)  = Weight_t(1)+Weight_fw(i);
end

for i = a+1:1:a+b
    Weight_pl(i) = Weight_pl(i-1)-PL_sub1;
    Weight_fw(i) = Weight_fw(i-1)+(abs(Weight_pl(i)-Weight_pl(i-1)));
    Weight_t(i)  = Weight_t(i-1);
end

for i = a+b+1:1:n
    Weight_pl(i) = Weight_pl(i-1)-PL_sub2;
    Weight_fw(i) = Weight_fw(i-1);
    Weight_t(i)  = Weight_t(i-1)-PL_sub2;
end

for i = 1:1:n
    [W_i, W_f, RF] = funct_weight_PLrange(OEW,Weight_pl(i),Weight_fw(i))
    [~,Range(i)]   = Breguet_singleMaAlt(Ma, Alt, efic, W_i, W_f);
    Weight_OEW(i)  = OEW;
    Weight_PL(i)   = OEW+Weight_pl(i);
    Weight_RF(i)   = Weight_PL(i)+RF;
end

%% Plotting


figure;
plot (Range, Weight_t, '-k','LineWidth',2); hold on
plot(Range, Weight_OEW, '--k','LineWidth',2)
plot(Range, Weight_PL, ':k','LineWidth',2)
plot(Range, Weight_RF, '-.k','LineWidth',2)
ylim ([0,MTOW+500])
xlim ([0,Range(n)+100])
xlabel ('Range [km]')
grid on

figure;
plot (Range, Weight_pl, '-k','LineWidth',2); grid on;
xlim ([0,Range(n)+100])
xlabel ('Range [km]')
ylabel ('Payload [kg]')
    
end

