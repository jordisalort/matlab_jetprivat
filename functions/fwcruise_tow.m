% This function calculates all weight parameters of an aircarft using
% different approximations to be specified throughout the script:
% MTOW, OEW, FW, MPL, MTOW/LTOW

function [MTOW_def,OEW_def,FW_total,Wi_cruise,Wf_cruise,ratio_mtow_ltow,MPL]=fwcruise_tow()

%% Inputs

loadConstants;    % Load different constants

Ma         = Ma_ref;         % Mach number at cruise phase
Num_engine = 2;              % Number of engines
Wengine    = Num_engine*200; % Engines weight

% Ascending parameters
v_climb       = 12.7*3.6;                 %km/s
v_avanc_climb = 130*3.6;                  %km/h 
altitude      = Altitud*(1/3.28084)/1000; %ft to meter to km

% Mission parameters
range_mission   = 2000;                            %km 
v_cruise        = Ma*a0*f_Theta_sqrt(Altitud)*3.6; %km/h
Range_alternate = 200;                             %km
t_seguretat     = 5/60;                            %hours

% Excel results importation
filname   = 'Mass_Private_Jet_3.xlsx';
sheet     = 1;
xlRange   = 'T22:U22';
data_coef = xlsread(filname,sheet,xlRange);
oew_mtow  = data_coef(1);
fw_mtow   = data_coef(2);

%% PL estimation

n_pax       = 6;     % Maximum nuumber of passengers
n_crew      = 1;     % Number of crew with max. passengers
weight_pax  = 91.13; % Estimated weight per passenger [kg]
weight_crew = 77;    % Estimated weight per crew member [kg]
baggage     = 13;    % Estimated baggage weight [kg]

PL    = n_pax*(weight_pax+baggage);   % Total passengers weight [kg]
WCREW = n_crew*(weight_crew+baggage); % Total crew weight [kg]

%% MPL estimation

rho_merc     = 997;                         % kg/m^3
rho_equipaje = baggage*1.2/(34.5*54*23e-6); % kg/m^3
kbd          = 0.35;                         
sb           = 147773e-6;                   % Cargo hold cross surface [m^2]
lbodega      = 7;                           % Cargo hold lentgh [m] 
Vb           = sb*lbodega ;                 % Cargo hold volume [m^3]

% Torenbeek estimation equation
MPL   = PL+WCREW+rho_merc*(kbd*Vb-(n_pax+n_crew)*baggage/rho_equipaje); 

%% First MTOW approximation

% Definition of initial parameters for the loop
error      = 1;
n          = 1;
tolerancia = 0.05;


% MTOW initial value using Torenbeek reference: 
MTOW_min = (PL-PL*0.1)/(1-oew_mtow-fw_mtow);

%% OEW calculation

% Fuselage geometry for Torenbeek's graph calculation (p.147)

bf       = 2.278;              % Height of fuselage. Not considering the lower part
hf       = 2.174;              % Width of fuselage
lf       = (max(bf,hf))*Sr;     % Lenght of fuselage
valor_x  = lf*(bf+hf)/2;
delta_we = 130.5*valor_x-1110; % By choosing two different points of the graph Fig 5-3

% Roskman constants (Source: p.47, tab. 2.15 )
A            = 0.2678;
B            = 0.9979;
correct_rosk = 0.4;    %Roskam's correction factor

%% Loop to find the MTOW

while error > tolerancia
    
n = n+1; % iterations counter

% OEW calculation using Torenbeek:
OEW(1) = 0.2*MTOW_min+500+delta_we+Wengine;

% OEW using Snorri Gudmundsson:
OEW(2) = MTOW_min*oew_mtow;

% OEW using Roskam:
OEW(3) = correct_rosk*(10^(A)*MTOW_min^(B));

% SECOND METHODOLOGY
OEW_MTOW_2 = 0.5371+0.0066*log(MTOW_min); %p.148 Snorri, but for turboprop.
OEW(4)     = MTOW_min*OEW_MTOW_2;

% Initial approach of FW calculation
FW(2) = fw_mtow*MTOW_min;

% Weight ratios for each flight phase
w_phases = [0.990;0.995;0.995;0.980;0;0;0.990;0;0.992];

w1 = w_phases(1)*MTOW_min; % Engines start and warm up
w2 = w_phases(2)*w1;       % Taxi
w3 = w_phases(3)*w2;       % Take-off

% Climb and accelerate to cruise conditions
t_climb     = altitude/v_climb;
range_climb = t_climb*v_avanc_climb; %km 
w4          = funct_FW(range_climb,w3,v_avanc_climb,0.8,13); 

% Weight ratio for cruise phase using Breguet analytical solution
w5 = funct_FW(range_mission+v_cruise*t_seguretat-range_climb,w4,v_cruise,cj_ref,efic);

% Loiter
E_ltr   = 0.75;  % Hours of loiter
c_j_ltr = 0.5;   %(lb to kg)/hour
L_D_ltr = 13;    % Aerodynamic efficiency for loiter
w6      = w5*exp(-E_ltr*c_j_ltr/L_D_ltr);
w7      = w_phases(7)*w6;                                     % Descent
w_res   = w7*exp(-E_ltr*c_j_ltr/(0.95*L_D_ltr));              % Reserve fuel for 45 more min.
w8      = funct_FW(Range_alternate,w_res,128,0.9*conv_cj,10); % Fly to alternate and descense
w9      =w_phases(9)*w8;                                      % Landing, taxi and shut-down

% Final weights obtained
FW(1)    = MTOW_min-w9;                 % Total fuel used
OEW_tent = MTOW_min-FW(1)-PL;           % OEW obtained
MTOW     = FW(1)+OEW_tent+PL+WCREW;     % MTOW obtained
error    = abs(OEW_tent-OEW(1))/OEW(1); % Relative error on the MTOW

MTOW_min = MTOW; % MTOW calculated value for future iteration

  if MTOW > 5.5e3
      break
  end

end

% FW recalculation
MTOW_def = MTOW;

w1    = w_phases(1)*MTOW_def;
w2    = w_phases(2)*w1;
w3    = w_phases(3)*w2;
w4    = funct_FW(range_climb,w3,v_avanc_climb,0.8,13);
w5    = funct_FW(range_mission+v_cruise*t_seguretat-range_climb,w4,v_cruise,0.75,11);
w6    = w5*exp(-E_ltr*c_j_ltr/L_D_ltr);
w7    = w_phases(7)*w6;
w_res = w7*exp(-E_ltr*c_j_ltr/(0.95*L_D_ltr));
w8    = funct_FW(Range_alternate,w_res,128,0.9*conv_cj,10);
w9    = w_phases(9)*w8;

% Final weights
FW(3)     = MTOW_def-w9;
OEW_def   = MTOW_def-FW(3)-PL-WCREW;
FW_total  = FW(3);
Wi_cruise = w4;
Wf_cruise = w5;

ratio_mtow_ltow = w8/MTOW;

end

% AUTHOR'S NOTES:
% -Note that the iteration depends on Roskam's proposed methodology by using
%Torenbeek's OEW approach
% The aim of the program is to be able to compare all the results togheter
%and to decide which is better to estimate the weight of the Jet Plane
% Some values may be reviewed for an accurate approximation, specially those
%related to the velocity. 
