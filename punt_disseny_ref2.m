clc
clear all
%INPUTS
%Atmosphere
altitud = 750; %msms (Benabarre)
rho_0 =1.225;
T=20-1*altitud/1000; %Atmosfera isa? (Buscar)
rho = rho_0*exp(-9.81*altitud/(287*(T+273))); %Adapt to the airports that we will operate
sigma=rho/rho_0; %Ratio densities
%Atmosphere
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
V_approach = 110/3.6; %m/s %From Arcus M ??
V_stall = V_approach/1.3; %See literature REVISAR!!!!!
CL_max = [1.6;2.0;1.6];  %See table at literature [clean;land;takeoff]
V_takeoff = 1.1*V_stall; %Literature
CL_takeoff = CL_max(3)/1.1^2; %Literature
S_land=400; %m, distance to land
V_stall_land=V_stall/1.2; %Buscar valor perqu� no en tinc nidea
%S_L=0.5847*V_stall_land^2; %Literature
MTOW=850;
WTOW=850*9.8; %Agafar com a funci�
FW=850*9.8; %Agafar funci�
lastre=60*9.8;
wl_wt0=(WTOW-lastre)/WTOW; %(funci�)
f=wl_wt0;  %Agafar el valor que toqui!!!
N=1;
CD0=0.02; %REVISAR!!!!
e = 0.8; %REVISAR!!!
c_v=0.04; %REVISAR!!! Constant climb gradient
c_ms=3; %Revisar%El m�nim EASA seria 1.5 %Constant climb rate
%------------------------
TOP = 110; %Sailplane and graph
w_s=linspace(0,800,0.5*(800-0));
%------------------------
K_l=0.2; %Revisar

%------------------------------------------------------------------------
%STALL SPEEDS
for i=1:1:2
w_s1(i)=0.5*rho*V_stall^2*CL_max(i);
end
ploty=[0;5];
w_s1plot(1,1)=w_s1(1);
w_s1plot(1,2)=w_s1(1);
w_s1plot(2,1)=w_s1(2);
w_s1plot(2,2)=w_s1(2);


%TAKEOFF 
p_w_takeoff=1/(TOP*CL_takeoff*sigma).*w_s; 

%LANDING
w_s_landing=K_l*CL_max(2)*sigma*(S_land/f);
for i=1:1:2
w_s_landing_plot(i)=w_s_landing;
end

%CLIMB RATE
n=4;
A=linspace(12,36,n);

for i=1:1:n  
    CD=CD0+CL_takeoff^2*(A(i))^(-1)/(pi*e);
    for j=1:1:400 %Recordar que surt de dalt
p_w_climb_rate(i,j)=c_ms/(sqrt((w_s(j)))*(sqrt(2/(rho*CL_takeoff))))+CD/(CL_takeoff);
%p_w_climb_rate(i,2)=c_ms/(sqrt((w_s(j)))*(sqrt(2/(rho*CL_takeoff))))+CD/(CL_takeoff);
    end
end
%CLIMB GRADIENT
%p_w_climb=(rho0/rho)^0.75*(N/(N-1))*
n=4;
A=linspace(12,36,n);
for i=1:1:n
p_w_climb(i,1)=(rho_0/rho)^0.75*(N/N)*(c_v+2*sqrt(CD0/(A(i)*e*pi)));
p_w_climb(i,2)=(rho_0/rho)^0.75*(N/N)*(c_v+2*sqrt(CD0/(A(i)*e*pi)));
end

plotx=[0;750];
%MOTORS ANALYIS (WATTS)
m1=260995/MTOW;
m2=648759/MTOW;
m3=37225/MTOW;
%MOTORS ANALYIS (HP)
m1=360/WTOW;
m2=87/WTOW;
m3=50/WTOW;

for i=1:1:2
    m1_plot(i)=3*m1;
    m2_plot(i)=3*m2;
    m3_plot(i)=3*m3;
end
%NOTE THAT CRUISE SPEED IS NOT PLOTTED BECAUSE THE MOTOR IS NOT GOING TO BE
%USED DURING CRUISE. The motor will only be used during take-off or for
%safety reasons.
hold on
plot(w_s1plot(1,:),ploty,'k--')
plot(w_s1plot(2,:),ploty,'k--')
plot(w_s,p_w_takeoff,'k-')
plot(w_s_landing_plot(:),ploty,'k--')
plot(w_s, p_w_climb_rate(1,:))
plot(w_s, p_w_climb_rate(2,:))
plot(w_s, p_w_climb_rate(3,:))
plot(w_s, p_w_climb_rate(4,:))
plot(plotx,m1_plot(:));
plot(plotx,m2_plot(:));
plot(plotx,m3_plot(:));

