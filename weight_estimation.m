%Weight estimation for the motor glider
%Excel results exportation
filname='Similar_Gliders.xlsx';
sheet = 1;
xlRange ='S36:T38';
data_coef = xlsread(filname,sheet,xlRange);
oew_mtow_elect = data_coef(1,1);
oew_mtow_combustion = data_coef(2,1);
oew_mtow_generic = data_coef(3,1);
fw_mtow_elect = data_coef(1,2);
fw_mtow_combustion = data_coef(2,2);
fw_mtow_generic = data_coef(3,2);

%PAYLOAD ESTIMATION
n_pax = 2;
weight_pax = 77; %77kg each passanger
%Can be changed and discussed:
baggage = 9; %9kg For similar sailplanes, for each passanger

PL = n_pax*(weight_pax+baggage);

%MTOW CALCULATION
%Torenbeek reference:
i=1;
opcions = 3;
for i=1:1:opcions
MTOW(i) = PL/(1-data_coef(i,1)-data_coef(i,2));
end

%MPL CALCULATION
%It is not useful for sailplane design
%Roskman constants (FONT: )
A=1;
B=1;
i=1;
for i =1:1:opcions;
%OEW CALCULATION (Torenbeek)
OEW(1,i)= 0.2*MTOW(i)+500+5 %Pendent
%Semejantes
OEW(2,i)=MTOW(i)*data_coef(i,1);
%Roskam
OEW(3,i)=A+B*log(MTOW(i)); %Pendent
end

%FW CALCULATION
%Flight profile


%For similar airplanes
xlRange2='O37:O37';
FW=xlsread(filname,sheet,xlRange2);

