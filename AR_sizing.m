clear all
%Sizing of the aspect ratio
b=15; %[m]
%Estimated parameters
v=75; %[m/s]
rho=1.225; %[kg/m^3]
CL=[0.2,0.6,1,1.4];
DP_prima=[0.03,0.04,0.05,0.06]; %[m^2]
AR=linspace(5,50,100);
e=1;
%Previous calculations
q=0.5*rho*v^2;
for i=1:4
    for j=1:100
        CD_P(i,j)=DP_prima(i)/b^2*AR(j);
        CDi(i,j)=CL(i)^2/(pi*AR(j)*e);
        CD(i,j)=CDi(i,j)+CD_P(j);
    end
    
end
hold on
plot(AR,CD(1,:))
plot(AR,CD(2,:))
plot(AR,CD(3,:))
plot(AR,CD(4,:))
