% This script calculates the sensitivity of the static delfection parameter
% on the radius equation

%% Inputs

n    = 12;                  % number of component in es vector
ny   = 0.35;
h_cg = 0.6;
k_sg = 1/3;
t    = 0.5;
es = linspace(0,0.4,n);



%% Equation

for i = 1:1:n
    r(i) = ny * h_cg * (1+4*k_sg*es(i)*h_cg/(t^2));
end

%% Plotting

figure;
plot (es,r)
grid on;
xlabel ('e_s [m]')
ylabel ('Radius [m]')

%% Sensitivity
Sensitivity = (r(n)-r(1))/(es(n)-es(1));
X = ['The sensitivity is: r/es = ',num2str(Sensitivity)];
f = msgbox(X);

