clear all;
clc;
addpath('./functions')

loadConstants;
n=10;
W_i=3900;        W_f=3100;
a=W_i;
b=W_f;
Alt=Altitud*0.3048;
efic=11;
Ma=0.6;

f=@(W) -a0*f_Theta_sqrt(Alt)*efic*(Ma_ref^(beta_ct))*(Ma^(1-beta_ct))/(g*c_j_ref*W);
%% Trapezi
f2=@(W) -2*a0*f_Theta_sqrt(Alt)*efic*(Ma_ref^(beta_ct))*(Ma^(1-beta_ct))/(g*c_j_ref*W^3);
for i=1:1:n
    y_max=0;
    W=linspace(a,b,i);
    for j=1:1:i
    y=f2(W(1,j));
        if abs(y)>y_max
            y_max=abs(y);
        end
    end
    error_t(1,i)=y_max*(b-a)^3/(12*i^2);
end
error_t=abs(error_t);
R_t = Trapezi(a,b,1000,f);
R_t_km=R_t/1000;
error_rel_t=error_t/R_t;
k_tft_km = R_t_km/log(a/b);
%% Simpson

f4=@(W) -24*a0*f_Theta_sqrt(Alt)*efic*(Ma_ref^(beta_ct))*(Ma^(1-beta_ct))/(g*c_j_ref*W^5);
for i=1:1:n
    y_max=0;
    W=linspace(a,b,i);
    for j=1:1:i
    y=f4(W(1,j));
    if abs(y)>y_max
        y_max=abs(y);
    end
    end
    error_s(1,i)=y_max*(b-a)^5/(180*i^4);
end
error_s=abs(error_s);
R_s = Simpsons(a,b,1000,f);
R_s_km=R_s/1000;
error_rel_s=error_s/R_s;
k_tfs_km = R_s_km/log(a/b);

%% Plotting

figure;
semilogy(error_rel_t);
hold on;
semilogy(error_rel_s);
legend('Trapezoidal Rule','Simpsons Rule')
xlabel('Order of discretization')
ylabel('Relative error')
grid on;
xlim([1 n])
hold off
